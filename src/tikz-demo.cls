\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tikz-demo}

\LoadClass[
	varwidth=\maxdimen,
	multi=page,
	border=1cm,
]{standalone}

\RequirePackage{tikz-highlight}

\RequirePackage{bookmark}
\RequirePackage{fontspec}
\RequirePackage{pgfplots}
\RequirePackage{tikz}

% Should be loaded last according to documentation
\RequirePackage{hyperref}

\hypersetup{
	pdfauthor={Jonston Chan},
	pdfkeywords={LaTeX; TikZ; PGF},
}

\usetikzlibrary{
	arrows.meta,
	backgrounds,
	graphs,
	shapes.misc,
}

\pgfplotsset{compat=1.10}

% NL = non-ligature
% This prevents ">=" becoming "≥" in code listings
\setmonofont{JetBrainsMonoNL}[
	Path=./fonts/JetBrainsMono-2.304/fonts/ttf/,
	Extension=.ttf,
	UprightFont=*-Regular,
	BoldFont=*-Bold,
]

\newcommand{\TikZ}{Ti\textit{k}Z}

\newcommand{\demo}[2]{
	\begin{page}
		\section{#1}

		\begin{center}
			\input{#2}
		\end{center}

		\lstinputlisting{#2}
	\end{page}
}
